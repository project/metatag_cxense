<?php

namespace Drupal\cxense\Plugin\metatag\Group;

use Drupal\metatag\Plugin\metatag\Group\GroupBase;

/**
 * Cxense.
 *
 * @MetatagGroup(
 *   id = "cxense",
 *   label = @Translation("cxense"),
 *   description = @Translation("Provides support for Cxense's custom meta tags."),
 *   weight = 4
 * )
 */
class Cxense extends GroupBase {
  // Inherits everything from Base.
}
